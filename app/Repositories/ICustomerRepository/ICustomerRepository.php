<?php

namespace App\Repositories\ICustomerRepository;

use Illuminate\Http\Request;

interface ICustomerRepository
{
    public function Get(Request $request);
}
