<?php

namespace App\Services;

use App\Repositories\IProductRepository\IProductRepository;
use App\Services\IProductService\IProductService;
use Illuminate\Http\Request;

class ProductService implements IProductService
{
    private readonly IProductRepository $_productRepository;

    public function __construct(IProductRepository $productRepository)
    {
        return $this->_productRepository = $productRepository;
    }

    public function Get(Request $request)
    {
        return $this->_productRepository->Get($request);
    }
}
