TestWork - Laravel REST API Application

1. Database of two tables with a many-to-many relationship
2. Repository pattern for managing the database
3. Simple key authentication for securing the API
4. Sorting and multi-field searching for the data
5. Utilizes pivot attributes for models and includes them in search queries

- Installation

1. Install dependencies
    - composer install

2. Configure the '.env' file with your database settings

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=testwork
    DB_USERNAME=root
    DB_PASSWORD=

3. Run the migrations to create tables in the database
    - php artisan migrate

4. Run the seeder to populate the database with sample data
    - php artisan db:seed

5. Start the server
    - php artisan serve

- API endpoints

1. /customers - returns a list of all customers
2. /products - returns a list of all products

- Search parametars (for API endpoints above)

    {
        "sort_by":"",
        "sort_direction":"",
        "name":"",
        "product_name":"",
        "description":"",
        "price":"",
        "location":"",
        "contact":""
    }

- API Key Authentication
    - You need to add 'API_KEY' in the '.env' file the api key value
    - To access the api calls, you need to put "X-Api-Key: [API_KEY]" in postman in headers