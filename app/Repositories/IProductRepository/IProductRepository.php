<?php

namespace App\Repositories\IProductRepository;

use Illuminate\Http\Request;

interface IProductRepository
{
    public function Get(Request $request);
}
