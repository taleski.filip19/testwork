<?php

namespace App\Dto\CustomerDto;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Builder;

class CustomerFilterDto
{
    public static function Filter(Request $request)
    {
        $sortBy = $request->sort_by;
        $sortDirection = $request->sort_direction;
        $name = $request->name;
        $productName = $request->product_name;
        $location = $request->location;
        $contact = $request->contact;
        $description = $request->description;
        $price = $request->price;

        $queryBuilder = Customer::with('products');

        if ($name) {
            $queryBuilder->where('name', 'like', '%' . $name . '%');
        }

        if ($productName) {
            $queryBuilder->whereHas('products', fn (Builder $query) =>
            $query->where('name', $productName));
        }

        if ($description) {
            $queryBuilder->whereHas('products', fn (Builder $query) =>
            $query->where('description', $description));
        }

        if ($price) {
            $queryBuilder->whereHas('products', fn (Builder $query) =>
            $query->where('price', '<', $price));
        }

        if ($location) {
            $queryBuilder->where('location', $location);
        }

        if ($contact) {
            $queryBuilder->where('contact', $contact);
        }

        return $queryBuilder->orderBy($sortBy ?: 'name', $sortDirection ?: 'asc')->get();
    }
}
