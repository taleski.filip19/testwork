<?php

namespace App\Dto\ProductDto;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;

class ProductFilterDto
{
    public static function Filter(Request $request)
    {
        $sortBy = $request->sort_by;
        $sortDirection = $request->sort_direction;
        $name = $request->name;
        $productName = $request->product_name;
        $location = $request->location;
        $contact = $request->contact;
        $description = $request->description;
        $price = $request->price;

        $queryBuilder = Product::with('customers');

        if ($name) {
            $queryBuilder->whereHas('customers', fn (Builder $query) =>
            $query->where('name', 'like', '%' . $name . '%'));
        }

        if ($productName) {
            $queryBuilder->where('name', $productName);
        }

        if ($location) {
            $queryBuilder->whereHas('customers', fn (Builder $query) =>
            $query->where('location', $location));
        }

        if ($contact) {
            $queryBuilder->whereHas('customers', fn (Builder $query) =>
            $query->where('contact', $contact));
        }

        if ($description) {
            $queryBuilder->where('description', $description);
        }

        if ($price) {
            $queryBuilder->where('price', '<', $price);
        }

        return $queryBuilder->orderBy($sortBy ?: 'name', $sortDirection ?: 'asc')->get();
    }
}
