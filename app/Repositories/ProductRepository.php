<?php

namespace App\Repositories;

use App\Dto\ProductDto\ProductFilterDto;
use App\Repositories\IProductRepository\IProductRepository;
use Illuminate\Http\Request;

class ProductRepository implements IProductRepository
{
    public function Get(Request $request)
    {
        return ProductFilterDto::Filter($request);
    }
}
