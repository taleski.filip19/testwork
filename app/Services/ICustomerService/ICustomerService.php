<?php

namespace App\Services\ICustomerService;

use Illuminate\Http\Request;

interface ICustomerService
{
    public function Get(Request $request);
}
