<?php

namespace App\Services\IProductService;

use Illuminate\Http\Request;

interface IProductService
{
    public function Get(Request $request);
}
