<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datasource = \Illuminate\Support\Facades\File::get(__DIR__ . "/json_data_source/customers.json");

        $products = json_decode($datasource, true);
        foreach ($products as $product) {
            Customer::create([
                'name' => $product['name'],
                'location' => $product['location'],
                'contact' => $product['contact']
            ]);
        }
    }
}
