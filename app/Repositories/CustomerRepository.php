<?php

namespace App\Repositories;

use App\Dto\CustomerDto\CustomerFilterDto;
use App\Repositories\ICustomerRepository\ICustomerRepository;
use Illuminate\Http\Request;

class CustomerRepository implements ICustomerRepository
{
    public function Get(Request $request)
    {
        return CustomerFilterDto::Filter($request);
    }
}
