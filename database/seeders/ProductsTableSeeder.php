<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datasource = \Illuminate\Support\Facades\File::get(__DIR__ . "/json_data_source/products.json");

        $products = json_decode($datasource, true);
        foreach ($products as $product) {
            Product::create([
                'name' => $product['name'],
                'description' => $product['description'],
                'price' => $product['price']
            ]);
        }
    }
}
