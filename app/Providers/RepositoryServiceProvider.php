<?php

namespace App\Providers;

use App\Repositories\CustomerRepository;
use App\Repositories\ICustomerRepository\ICustomerRepository;
use App\Repositories\IProductRepository\IProductRepository;
use App\Repositories\ProductRepository;
use App\Services\CustomerService;
use App\Services\ICustomerService\ICustomerService;
use App\Services\IProductService\IProductService;
use App\Services\ProductService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Customer provider
        $this->app->bind(ICustomerService::class, CustomerService::class);
        $this->app->bind(ICustomerRepository::class, CustomerRepository::class);

        // Product provider
        $this->app->bind(IProductRepository::class, ProductRepository::class);
        $this->app->bind(IProductService::class, ProductService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
