<?php

namespace App\Dto\ResponseModelDto;

use Illuminate\Http\JsonResponse;

class ResponseModelDto
{
    public static function Success($Data, $Code): JsonResponse
    {
        $res = [
            'success' => true,
            'message' => 'Successful get data.',
            'data' => $Data,
        ];
        return response()->json($res, $Code);
    }
}
