<?php

namespace App\Services;

use App\Repositories\ICustomerRepository\ICustomerRepository;
use App\Services\ICustomerService\ICustomerService;
use Illuminate\Http\Request;

class CustomerService implements ICustomerService
{
    private readonly ICustomerRepository $_customerRepository;

    public function __construct(ICustomerRepository $customerRepository)
    {
        return $this->_customerRepository = $customerRepository;
    }

    public function Get(Request $request)
    {
        return $this->_customerRepository->Get($request);
    }
}
