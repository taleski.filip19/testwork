<?php

namespace App\Http\Controllers;

use App\Dto\ResponseModelDto\ResponseModelDto;
use App\Services\ICustomerService\ICustomerService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    private readonly ICustomerService $_customerService;

    public function __construct(ICustomerService $customerService)
    {
        $this->_customerService = $customerService;
    }

    public function Get(Request $request)
    {
        $data = $this->_customerService->Get($request);
        return ResponseModelDto::Success($data, 200);
    }
}
