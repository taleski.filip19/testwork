<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class KeyAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $apiKey = $request->header('X-Api-Key');

        if (!$apiKey) {
            return $this->sendUnauthorizedResponse('API key is missing.');
        }

        if ($apiKey !== env('API_KEY')) {
            return $this->sendUnauthorizedResponse('API key is invalid.');
        }

        return $next($request);
    }

    /**
     * Send an unauthorized response with the given error message.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendUnauthorizedResponse(string $message): JsonResponse
    {
        return response()->json(['error' => 'Unauthorized: ' . $message], 401);
    }
}
