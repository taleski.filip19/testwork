<?php

namespace App\Http\Controllers;

use App\Dto\ResponseModelDto\ResponseModelDto;
use App\Services\IProductService\IProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private readonly IProductService $_productService;

    public function __construct(IProductService $productService)
    {
        $this->_productService = $productService;
    }

    public function Get(Request $request)
    {
        $data = $this->_productService->Get($request);
        return ResponseModelDto::Success($data, 200);
    }
}
